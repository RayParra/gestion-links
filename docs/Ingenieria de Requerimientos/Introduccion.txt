INTRODUCCION
    En la actualidad la era digital en el mundo tiene gran importancia, lo mismo podemos decir para la informacion
la cual resulta el activo mas importante de las empresas y de muchas personas. En este mismo sentido, las personas
necesitan investigar en las diversas fuentes y recursos de informacion los diferentes objetos de aprendizaje
que necesitan para fortalecer sus skills. Por la diversidad de recursos y objetos de aprendizaje que exisen, las
personas se ven envueltos en la decision de seleccionar los mejores objetos que cubran sus necesidades de aprendizaje.

En el sentido de fuentes y recursos de informacion se entiende cualquier instrumento que nos pueda servir para satisfacer
una necesidad informativa o de aprendizaje. El objetivo de las fuentes y recursos de información será facilitar la 
localización e identificación de obejtos de aprendizaje. Así surge la necesidad de responder a la pregunta ¿dónde vamos
a buscar la información? Antes de responder la pregunta es necesario considerar el tipo de fuentes y recursos de información 
que se pueden consultar para encontrar los diferentes objetos de aprendizaje que se necesitan en el estudio autodirigido. 
Debido a la gran cantidad de objetos de aprendizaje que existen en las diferentes fuentes y recursos las personas deben 
seleccionar con mucho cuidado recursos que le proporcionen la información adecuada a sus necesidades.

Debido a esa gran cantidad de objetos que exisnten en las diversas fuentes, las personas adquieren diferentes niveles de 
frustracion o abrumacion. Por lo que no se sienten capaces de seleccionar los objetos de aprendizaje mas adecuados a sus 
necesidades. En ocaciones es dificil tomar decisiones de este tipo.

Por otro lado, el proceso de toma de decisiones, conocido como ‘decision making’, es un método que consiste en reunir la 
información, evaluar alternativas y, luego, tomar la mejor decisión final posible. Tomamos decisiones a diario. Cuando decidimos
si iremos en autobús al trabajo o en taxi. ¿Helado de vainilla o de chocolate? y otras decisiones comunes, pero cuando se trata
de aumentar nuestras habilidades siempre queremos obtener la mejor informacion o los mejores objetos de aprendizaje posibles,
entonces aqui es donde las decisiones pueden ser dificil de tomar. La validez en este tipo de decisiones, depende de la calidad de 
la información con la que se cuenta, en ese sentido es necesario tener un buen proceso que te ayude a evaluar los recursos y tomar 
la mejor decision. 

Entonces ¿Como podemos realizar el proceso para la toma de decisiones?, existen diferentes modelos que te ayudan a generar el 
proceso adecuado para la toma de decisiones, por ejemplo: Los modelos Racionales, los modelos Intuitivos y los modelos Creativos.

Los diferentes modelos para la toma de decisiones tienen el siguiente proceso:
Cuando estés identificando la decisión, el primer paso es hacerte las siguientes preguntas:

Paso 1. 
Cuando estés identificando la decisión, el primer paso es hacerte las siguientes preguntas:
¿Qué problema hay que resolver?
¿Cuál es el objetivo que piensas alcanzar con la implementación de esta decisión?
¿Cómo medirás el éxito?
Todas estas preguntas sirven como técnicas para definir objetivos comunes que, a corto plazo o a largo plazo, te ayudarán para que 
se te ocurran posibles soluciones. Cuando la definición del problema está clara, después tienes más información disponible para 
elaborar mejores decisiones con las cuales abordar la resolución de problemas.

Paso 2 Reúne información relevante.
El paso de reunir la información relacionada con la decisión que se quiere tomar es fundamental para tomar decisiones basadas en 
información. ¿Tu equipo tiene datos históricos que se relacionen con este problema? ¿Alguien ha intentado resolverlo antes? También 
es importante buscar información fuera del equipo o de la empresa. Para lograr un proceso de decisión efectivo es necesario contar 
con información de varias fuentes diferentes. Busca fuentes externas, como pueden ser investigaciones sobre el mercado, trabajar con 
consultores o hablar con colegas de diferentes empresas que tengan experiencia relevante para el caso. La información adicional que 
reúnas le servirá a tu equipo para identificar distintas alternativas de soluciones para el problema.


Paso 3: Identifica soluciones alternativas

En este paso es necesario que busques varias soluciones diferentes para el problema en cuestión. Es muy importante encontrar más de una
alternativa posible en los casos en que se toman decisiones de negocios, porque las distintas personas involucradas pueden tener 
necesidades diferentes dependiendo del rol que cumplan. Por ejemplo, si en una empresa se busca una herramienta de gestión del trabajo, 
el equipo de diseño puede tener necesidades diferentes de las del equipo de desarrollo. Elegir una sola solución o tomar decisiones 
individuales sin pensarlo demasiado puede no ser conveniente en absoluto.


Paso 4: Sopesa las soluciones

Es la instancia en que tomas todas las soluciones diferentes que se les han ocurrido y analizas con un pensamiento estratégico cómo 
tratarías el problema inicial. El equipo empieza a identificar las ventajas y desventajas de cada opción y a eliminar alternativas 
relacionadas con esas opciones en pos de poder evaluar las posibles consecuencias.

Hay algunas formas comunes en que tu equipo puede analizar y sopesar las opciones:

Lista de ventajas y desventajas
Análisis FODA
Matriz de decisiones


Paso 5: Elige una de las alternativas

El paso siguiente es tomar una decisión final. Considera toda la información que has reunido y de qué forma puede afectar la decisión a 
las diferentes personas.
A veces, la decisión correcta no es una de las alternativas, sino que lo mejor es combinar diferentes opciones. Las decisiones estratégicas
efectivas abarcan la resolución creativa de problemas y tener imaginación, para no limitarte a elegir con tu equipo solamente opciones 
obvias.
Uno de los valores clave de Asana es no aceptar soluciones por compromiso. A veces, la elección de una solución única puede implicar que se 
pierdan los beneficios de las demás. Si puedes, intenta buscar opciones que superen las alternativas presentadas.


Paso 6: Ponte en acción

Una vez que el responsable de la toma de decisión da luz verde, habrá llegado la hora del curso de acción y poner la solución en práctica.
Tómate el tiempo necesario para generar un plan para la implementación, de modo que el equipo esté al tanto de los pasos a seguir. Después,
deberás poner el plan en práctica y supervisar los avances para determinar si la decisión ha sido acertada o no.


Paso 7: Revisa la decisión tomada y su impacto (tanto lo bueno como lo malo)

Una vez que hayas tomado una decisión, podrás supervisar todo según las métricas de éxito definidas en el paso 1. Así es como evaluarás los
resultados obtenidos y se determinará si la solución cumple realmente con el criterio de éxito del equipo o no.

A continuación, compartimos algunas preguntas a tener en cuenta al momento de revisar la decisión:

¿Se resolvió el problema que el equipo identificó en el paso 1? 
¿La decisión afectó al equipo de manera positiva o negativa?
¿Quiénes se beneficiaron con esta decisión? ¿Qué otras personas se vieron afectadas de forma negativa?
Si la solución no ha sido la mejor opción, el equipo puede aplicar técnicas de gestión iterativa de proyectos como ayuda. Les permitirá 
adaptarse rápidamente a los cambios y tomar las mejores decisiones con los recursos que tienen.







