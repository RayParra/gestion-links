PROBLEMATICA

El gran problema de la informacion en el internet es que existen muchas opciones, con esto no se dice que sea algo terrible o 
inservible. Se trata de explicar que al buscar informacion en ocaciones existen indecisiones por la informacion que se quiere
elegir. Entonces tenemos que atravesar por todo un proceso para tomar esas decisiones, incluso a pesar de que pueden ser 
elecciones simples y fáciles. Con esto no queremos decir que las decisiones no son tan simples como elegir lo que uno quiere. 

Basados en este pequeño analisis se determina que buscar y seleccionar los recursos necsarios para el aprendizaje se vuelve
dificil debido a la inhundacion de objetos de aprendizaje y recursos que existen en las diferentes fuentes de informacion, esto
causa un gran nivel de frustracion y abrumacion, lo cual puede derivar en procastinacion o en decidia. En cualquier forma esto
puede ser fatal para muchas personas que buscan objetos de aprendizaje  en las diferentes fuentes y recursos de informacion.